package palindrome.core;

import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;

public class StringInvestigator {
	
	/**
	 * method to detect if a string is a palindrome.
	 *  Palindrome is a word, phrase, number, or other sequence of characters which reads 
	 *  the same backward as forward.
	 *  This method do not consider empty words or one character words to be a palindrome
	 * @param string to be checked
	 * @return true if this string is a palindrome, false if not.
	 */
	public boolean isPalindrome(String string) {
		if(string.length() < 2) return true;
		else if(string.length() == 2 ) return string.charAt(0) == string.charAt(1);
		else return (string.charAt(0) == string.charAt(string.length()-1)) && 
				isPalindrome(string.substring(1, string.length()-1));
	}
	
	/**
	 * This method takes a string and return a list of all the palindromes which this string contain.
	 * That include the whole string, any substring larger then one character length but no duplicates.
	 * @param string to check
	 * @return all possible palindromes in the given string
	 */
	public Set<String> getAllSubPalindromes(String s) {
		Set<String> result = new HashSet<>();
		for(int i=0; i<s.length(); i++) {
			for(int j=i+1; j<=s.length(); j++) {
				String inTest = s.substring(i,j);
				if((j-i) > 1 && isPalindrome(inTest)) {
					result.add(inTest);
				}
			}
		}
		return result;
	}
	
	/**
	 * This comparator compare strings by their length and use to compare palindromes sizes
	 * @author nshoef
	 *
	 */
	public static class PalindromeComperator implements Comparator<String> {

		@Override
		public int compare(String o1, String o2) {
			return o1.length() - o2.length();
		}

	}

}
