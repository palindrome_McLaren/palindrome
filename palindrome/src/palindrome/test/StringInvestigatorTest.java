/**
 * 
 */
package palindrome.test;

import static org.junit.Assert.*;

import org.junit.Test;

import palindrome.core.StringInvestigator;

/**
 * @author nshoef
 *
 */
public class StringInvestigatorTest {

	/**
	 * Test method for {@link palindrome.core.StringInvestigator#isPalindrome(java.lang.String)}.
	 */
	@Test
	public void testIsPalindrome() {
		StringInvestigator pi = new StringInvestigator();
		assertTrue(pi.isPalindrome("H"));
		assertTrue(pi.isPalindrome(""));
		assertTrue(pi.isPalindrome("ll"));
		assertTrue(pi.isPalindrome("aba"));
		assertTrue(pi.isPalindrome("AGDGA"));
		assertTrue(pi.isPalindrome("AsdFdsA"));
		assertTrue(pi.isPalindrome("2!!2"));
		assertTrue(pi.isPalindrome("9876543210123456789"));
		
		assertFalse(pi.isPalindrome("asdgfd"));
		assertFalse(pi.isPalindrome("AGDA"));
		assertFalse(pi.isPalindrome("AGgaAGaga"));
		assertFalse(pi.isPalindrome("<AABBAA>"));
		assertFalse(pi.isPalindrome("ab"));
		assertFalse(pi.isPalindrome("*()*&^"));
		
	}

	/**
	 * Test method for {@link palindrome.core.StringInvestigator#getAllSubPalindromes(java.lang.String)}.
	 */
	@Test
	public void testGetAllSubPalindromes() {
		StringInvestigator pi = new StringInvestigator();
		String a = "adasdfvuioiuvuu";
		assertEquals(6, pi.getAllSubPalindromes(a).size());
		String b = "adaada";
		assertEquals(4, pi.getAllSubPalindromes(b).size());
		String c = "ada";
		assertEquals(1, pi.getAllSubPalindromes(c).size());
		String d = "dffdgtyhdfds";
		assertEquals(3, pi.getAllSubPalindromes(d).size());
		String e = "";
		assertEquals(0, pi.getAllSubPalindromes(e).size());
		String f = "dfghjkl";
		assertEquals(0, pi.getAllSubPalindromes(f).size());
	}
	
	/**
	 * Test method for {@link palindrome.core.StringInvestigator.PalindromeComperator#compare(java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testCompare() {
		StringInvestigator.PalindromeComperator pc = new StringInvestigator.PalindromeComperator();
		assertEquals(0, pc.compare("ASDFDSA", "FDSASDF"));
		assertTrue(0 > pc.compare("sdf", "FDfgF"));
		assertTrue(0 < pc.compare("A45hbbh", "bgfh"));	
	}

}
