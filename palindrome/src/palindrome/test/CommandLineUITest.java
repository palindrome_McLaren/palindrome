/**
 * 
 */
package palindrome.test;

import static org.junit.Assert.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import org.junit.Before;
import org.junit.Test;

import palindrome.UI.CommandLineUI;
import palindrome.core.StringInvestigator;

/**
 * @author nshoef
 *
 */
public class CommandLineUITest {
	
	private StringInvestigator investigator;
	private InputStream input;
	
	@Before
	public void setParam(){
		investigator = new StringInvestigator();
	}

	/**
	 * Test method for {@link palindrome.UI.CommandLineUI#run()}.
	 * This test use an input file to replicate the user input.
	 * @throws FileNotFoundException if the test file is not in place.
	 */
	@Test
	public void testRun() throws FileNotFoundException {
		input = new FileInputStream("./src/palindrome/test/input2");
		CommandLineUI ui = new CommandLineUI(input, investigator);
		ui.run();	
	}

	/**
	 * Test method for {@link palindrome.UI.CommandLineUI#getStringFromUser()}.
	 * This test use an input file to replicate the user input.
	 * @throws FileNotFoundException if the test file is not in place.
	 */
	@Test
	public void testGetStringFromUser() throws FileNotFoundException {
		input = new FileInputStream("./src/palindrome/test/input1");
		CommandLineUI ui = new CommandLineUI(input, investigator);
		String s1 = ui.getStringFromUser();
		assertEquals("asdfgh", s1);
		String s2 = ui.getStringFromUser();
		assertEquals("1sdf%%$�%$kjhfdg,lkj", s2);
	}
}
