package palindrome.UI;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

import palindrome.core.StringInvestigator;

/**
 * This is a simple command line implementation of th UI interface.
 * It let the user enter a string and print the 3 largerst palindromes in the string.
 * @author nshoef
 *
 */
public class CommandLineUI implements UI {
	private final String WELCOME = "Welcome to palindrome app. this application allows you to"
			+ " investigate palindomes\n";
	private final String COMMANDS = "Please choose a command from the followings(enter the number):\n "
		+ "1. Get the 3 largest palindromes in a String\n 2. Exit";
	private final String GOODBYE = "You have choose to terminate, come back soon! <3";

	private final String[] VALID_KEYS = new String[]{"1","2"};
	private Scanner scan;
	private StringInvestigator investigator;

	public CommandLineUI(InputStream scanner, StringInvestigator investigator){
		scan = new Scanner(scanner);
		this.investigator = investigator;
	}

	/**
	 * This method start the UI
	 */
	@Override
	public void run() {
		System.out.println(WELCOME);
		while(true) {
			System.out.println(COMMANDS);
			String choice = scan.nextLine();
			while(!validateChoice(choice)) {
				System.out.println(COMMANDS);
				choice = scan.nextLine();
			}
			switch(choice) {
				case "1": getLargestPalindrome();
				break;
				case "2": terminate();
				break;
				default: throw new IllegalStateException(); //should not happen as we validate the string
			}
		}
	}

	/**
	 * Ask the user for a string and process the result
	 */
	private void getLargestPalindrome() {
		String input = getStringFromUser();
		printLargestPalindromes(3, input);
	}

	/**
	 * This method ask from the user a string.
	 * It validate that the user input only one string (ask to input again if not).
	 * @return the string input by the user
	 */
	public String getStringFromUser() {
		System.out.println("Please enter a string to check:");
		while (true) {
			String result = scan.nextLine();
			if(result.split(" ").length == 1) {
				return result;
			}
			System.out.println("You have entered more than one String, please try again:");
		}
	}

	/**
	 * This method takes a string, find all it's palindromes and print the n largest.
	 * If there are no palindromes in the string an appropriate message is printed. 
	 * If there are less palindromes then n, a message is printed together with all the
	 * existing palindromes.
	 * @param num, the n number of palindromes to print.
	 * @param string the string to check for palindromes
	 */
	public void printLargestPalindromes(int num, String string) {
		Set<String> palindroms = investigator.getAllSubPalindromes(string);
		if(palindroms.isEmpty()) { //no palindromes
			System.out.println("The string you have entered contain no Palindromes\n");
			return;
		}
		if(palindroms.size() < num) { //less palindromes then n
			System.out.println("There are only "+palindroms.size()+" palindromes in the string you have entered:\n");
			num = palindroms.size();
		}
		List<String> result = new ArrayList<>();
		Comparator<String> comparator = new StringInvestigator.PalindromeComperator();
		for(int i=0; i<num; i++) {
			String max = Collections.max(palindroms, comparator);
			result.add(max);
			palindroms.remove(max);
		}
		result.sort(Collections.reverseOrder(comparator));
		result.forEach(x -> System.out.printf("Text: %s, Index: %s, Length: %s\n\n",x, string.indexOf(x), x.length()));	
	}

	/**
	 * this is a helper method to validate the user input
	 * @param choice the user input
	 * @return true is the input is valid, false if not
	 */
	private boolean validateChoice(String choice) {
		return Arrays.asList(VALID_KEYS).contains(choice.trim());
	}

	/**
	 * This method terminate the application
	 */
	private void terminate() {
		System.out.println(GOODBYE);
		scan.close();
		System.exit(0);
	}
	
	public static void main(String[] args) {
		UI api = new CommandLineUI(System.in, new StringInvestigator());
		api.run();
	}
}
